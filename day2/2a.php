<?php

require_once "all_boxes.php";

function calculate_wrapping_paper($all_boxes)
{
    /** The [\s] pattern is for whitespace **/

    $all_converted_boxes = preg_split("[\s]",$all_boxes);

    $total_paper = 0;

    foreach ($all_converted_boxes as $box)
    {
        $dimensions = explode("x",$box);
        list($l,$w,$h) = $dimensions;

        /** The wrapping paper for every box accumulated to the @total_paper **/

        $total_paper += 2*$l*$w + 2*$w*$h + 2*$h*$l;

        /** Check which is the smallest side for every box and add it to the @total_paper **/

        if($l >= $w && $l >=$h)
        {
            $total_paper += $w * $h;
        }
        elseif ($w >= $h && $w >= $l)
        {
            $total_paper += $l * $h;
        }
        elseif ($h >= $l && $h >= $w)
        {
            $total_paper += $l * $w;
        }

    }

    return $total_paper;
}

echo  calculate_wrapping_paper($all_boxes);