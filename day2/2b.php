<?php

require_once "all_boxes.php";

function calculate_ribbon($all_boxes)
{
    /** The [\s] pattern is for whitespace **/

    $all_converted_boxes = preg_split("[\s]",$all_boxes);

    $total_ribbon = 0;

    foreach ($all_converted_boxes as $box)
    {
        $dimensions = explode("x",$box);
        list($l,$w,$h) = $dimensions;

        /** Check which is the smallest side for every box and add it to the @total_ribbon **/

        if($l >= $w && $l >=$h)
        {
            $total_ribbon += 2*$w + 2*$h;
        }
        elseif ($w >= $h && $w >= $l)
        {
            $total_ribbon += 2*$l + 2*$h;
        }
        elseif ($h >= $l && $h >= $w)
        {
            $total_ribbon += 2*$l + 2*$w;
        }

        /** The ribbon for every box accumulated to the @total_ribbon **/
        $total_ribbon += $l * $h * $w;

    }

    return $total_ribbon;
}

echo  calculate_ribbon($all_boxes);
