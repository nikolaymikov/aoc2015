<?php

require_once "instructions.php";

function calculate_total_brightness($instructions)
{
    $instructions = explode( "\n",$instructions);

    $total_brightness = 0;

    foreach ($instructions as $instruction)
    {
        //get the command
        if(strpos($instruction,"turn on")  !== false)
        {
            $instruction_state = "turn on";
        }
        elseif (strpos($instruction,"turn off")  !== false)
        {
            $instruction_state = "turn off";
        }
        else
        {
            $instruction_state = "toggle";
        }

        //get the coordinates
        $all_coordinates = explode(" ",$instruction);

        if($instruction_state == "toggle")
        {
            $coordinate_pairs = explode(",","$all_coordinates[1],$all_coordinates[3]");
        }
        else
        {
            $coordinate_pairs = explode(",","$all_coordinates[2],$all_coordinates[4]");
        }

        for($i = $coordinate_pairs[0]; $i <= $coordinate_pairs[2]; $i++)
        {
            for ($j = $coordinate_pairs[1]; $j <= $coordinate_pairs[3]; $j++)
            {
                if($instruction_state == "toggle")
                {
                    $total_brightness += 2;
                }
                elseif($instruction_state == "turn on")
                {
                    $total_brightness++;
                }
                else
                {
                    if($total_brightness != 0)
                    {
                        $total_brightness --;
                    }
                    else
                    {
                        $total_brightness = 0;
                    }
                }
            }
        }
    }
    return $total_brightness;
}

echo calculate_total_brightness($instructions);