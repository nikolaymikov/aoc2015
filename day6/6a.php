<?php

require_once "instructions.php";


function find_all_lit_lights($instructions)
{
    $instructions = explode( "\n",$instructions);

    $counter_lights = 0;

    $grid = [[]];

    for($a=0; $a < 1000; $a++)
    {
        for ($b=0; $b < 1000; $b++)
        {
            $grid[$a][$b]= -1;
        }
    }

    foreach ($instructions as $instruction)
    {
        if(strpos($instruction,"turn on")  !== false)
        {
            $instruction_state = "turn on";
        }
        elseif (strpos($instruction,"turn off")  !== false)
        {
            $instruction_state = "turn off";
        }
        else
        {
            $instruction_state = "toggle";
        }

        $all_coordinates = explode(" ",$instruction);

        if($instruction_state == "toggle")
        {
            $coordinate_pairs = explode(",","$all_coordinates[1],$all_coordinates[3]");
        }
        else
        {
            $coordinate_pairs = explode(",","$all_coordinates[2],$all_coordinates[4]");
        }

        for($i = $coordinate_pairs[0]; $i <= $coordinate_pairs[2]; $i++)
        {
            for ($j = $coordinate_pairs[1]; $j <= $coordinate_pairs[3]; $j++)
            {
                $light_bulb = $grid[$i][$j];

                if($instruction_state == "toggle")
                {
                    $grid[$i][$j] = $grid[$i][$j] * -1;
                }
                elseif($instruction_state == "turn on")
                {
                   $grid[$i][$j] = 1;
                }
                else
                {
                    $grid[$i][$j] = -1;
                }

                if($light_bulb == $grid[$i][$j])
                {
                    $counter_lights += 0;
                }
                elseif ($light_bulb > $grid[$i][$j] )
                {
                    $counter_lights += -1;
                }
                else
                {
                    $counter_lights += 1;
                }

            }
        }

    }
    return $counter_lights;
}


echo find_all_lit_lights($instructions);