<?php

require_once "given_directions.php";

define("UP","(");
define("DOWN",")");

function calculate_given_floor($given_directions)
{

    $floor = 0;

    $all_directions = str_split($given_directions);

    foreach ($all_directions as $direction)
    {
        if($direction == UP)
        {
            $floor++;
        }
        elseif($direction == DOWN)
        {
            $floor--;
        }
    }

    return $floor;
}

echo calculate_given_floor($given_directions);
