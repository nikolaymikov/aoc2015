<?php

require_once "given_directions.php";

define("UP","(");
define("DOWN",")");

function find_basement_direction($given_directions)
{
    $character_position = 0;
    $floor = 0;

    $all_directions = str_split($given_directions);

    foreach ($all_directions as $direction)
    {
        $character_position++;

        if($direction == UP)
        {
            $floor++;
        }
        elseif($direction == DOWN)
        {
            $floor--;
        }

        if($floor == -1)
        {
             break;
        }
    }
    return $character_position;
}

echo find_basement_direction($given_directions);