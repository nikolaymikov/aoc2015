<?php

require_once "instructions.php";

function find_signal_to_wire_a($instructions)
{
    $instructions = explode( "\n",$instructions);

    $signals = [];
    foreach ($instructions as $instruction)
    {

        $wires = explode("->",$instruction);
        $wire_sender = trim($wires[0]);
        $wire_receiver = "";
        if(isset($wires[1]))
        {
            $wire_receiver = trim($wires[1]);
        }

        if(!empty($wire_receiver))
        {
            $operands = explode(" ",$wire_sender);

            //check which operator to use
            if(strpos($wire_sender,'AND') !== false)
            {
                list($operand_left,,$operand_right) = $operands;
                if(is_numeric($operand_left))
                {
                    $result = $operand_left & $signals[$operand_right];
                }
                else
                {
                    $result = $signals[$operand_left] & $signals[$operand_right];
                }

            }
            elseif (strpos($wire_sender,'OR') !== false)
            {
                list($operand_left,,$operand_right) = $operands;
                $result = $signals[$operand_left] | $signals[$operand_right];
            }
            elseif (strpos($wire_sender,'LSHIFT') !== false)
            {
                list($operand_left,,$operand_right) = $operands;

                $result = $signals[$operand_left] << (int)$operand_right;
            }
            elseif (strpos($wire_sender,'RSHIFT') !== false)
            {
                list($operand_left,,$operand_right) = $operands;
                $result = $signals[$operand_left] >> (int)$operand_right;
            }
            elseif (strpos($wire_sender,'NOT') !== false)
            {
                list(,$operand_right) = $operands;
                $result = 65535 - ($signals[$operand_right]);
            }
            else
            {
                list($operand_right) = $operands;
                $result = (int)$operand_right;
            }

            $signals[$wire_receiver] = $result;
        }

    }

    ksort($signals);

    return $signals;
}

var_dump(find_signal_to_wire_a($instructions));

