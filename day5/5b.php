<?php

require_once "all_strings.php";

/**
 * check if the string contains
 * repeating pairs of letters
 * like: qt...qt
 **/
function count_letter_pairs($string)
{
    preg_match_all('/(..).*\1/', $string, $matches);

    if(!empty($matches[1]))
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * check if the string contains
 * repeating letters with a letter in the middle
 * like: zxz hvh
 **/
function count_repeating_letters_with_middle_letter($string)
{
    preg_match_all('/([a-z])\w\1/',$string,$matches);

    return count($matches[0]);
}

function calculate_all_nice_strings($all_strings)
{
    $nice_strings_counter = 0;
    foreach (str_split($all_strings,17) as $string)
    {
        if(count_letter_pairs($string) == true && count_repeating_letters_with_middle_letter($string) >=1 )
        {
            $nice_strings_counter ++;
        }
    }
    return $nice_strings_counter;
}

echo calculate_all_nice_strings($all_strings);