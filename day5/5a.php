<?php

require_once "all_strings.php";

function count_vowels($string)
{
    //check if the string contains any vowels
    return  substr_count($string,'a')+
        substr_count($string,'e')+
        substr_count($string,'i')+
        substr_count($string,'o')+
        substr_count($string,'u');
}

function count_double_letters($string)
{
    //check if the string contains substrings like: aa bb cc dd
    preg_match_all('/([a-z])\1/', $string, $matches);

    return count($matches[0]);
}

function count_disallowed_substrings($string)
{
    //check if the string contains any of the substrings
    if(preg_match("/(ab|cd|pq|xy)/i",$string))
    {
        return true;
    }
    else
    {
        return  false;
    }
}

function calculate_all_nice_strings($all_strings)
{
    $nice_strings_counter = 0;
    foreach (str_split($all_strings,17) as $string)
    {
        if(count_vowels($string) >= 3 && count_double_letters($string) >= 1 && count_disallowed_substrings($string) == false)
        {
            $nice_strings_counter ++;
        }
    }
    return $nice_strings_counter;
}

echo calculate_all_nice_strings($all_strings);