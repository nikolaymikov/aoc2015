<?php

require_once "all_moves.php";

define("NORTH","^");
define("SOUTH","v");
define("EAST",">");
define("WEST","<");

function find_number_of_houses($all_moves)
{
    $all_moves =  str_split($all_moves);

    $visited_houses_santa = [];
    $visited_houses_robo = [];

    $sx = $sy = $rx = $ry =  0;

    foreach ($all_moves as $key=>$move)
    {
        if($key % 2 == 0)
        {
            if($move == NORTH)
            {
                $sy++;
            }
            elseif ($move == SOUTH)
            {
                $sy--;
            }
            elseif ($move == EAST)
            {
                $sx++;
            }
            elseif ($move == WEST)
            {
                $sx--;
            }
        }
        else
        {
            if($move == NORTH)
            {
                $ry++;
            }
            elseif ($move == SOUTH)
            {
                $ry--;
            }
            elseif ($move == EAST)
            {
                $rx++;
            }
            elseif ($move == WEST)
            {
                $rx--;
            }
        }

        $visited_houses_santa[$sx.','.$sy] = 1;
        $visited_houses_robo[$rx.','.$ry] = 1;
    }

   return count($visited_houses_santa + $visited_houses_robo);
}


echo find_number_of_houses($all_moves);

