<?php

require_once "all_moves.php";

define("NORTH","^");
define("SOUTH","v");
define("EAST",">");
define("WEST","<");

function find_number_of_houses($all_moves)
{
    $all_moves =  str_split($all_moves);

    $visited_houses = [];
    $x = 0;
    $y = 0;

    foreach ($all_moves as $move)
    {
       if($move == NORTH)
       {
           $y++;
       }
       elseif ($move == SOUTH)
       {
            $y--;
       }
       elseif ($move == EAST)
       {
            $x++;
       }
       elseif ($move == WEST)
       {
            $x--;
       }
       $visited_houses[] = "{$x},{$y}";
    }
    return count(array_unique($visited_houses));
}


echo find_number_of_houses($all_moves);


