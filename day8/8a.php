<?php

/**
 * @Author Nikolay Mikov
 */

$all_file_lines = file("input.txt");

$total_characters_code = 0;
$total_characters_memory = 0;

foreach ($all_file_lines as $line)
{
    $cleaned_line = trim($line);
    $total_characters_code += strlen($cleaned_line);

    $cleaned_quotes_line = trim($cleaned_line,'"');
    $total_characters_memory += strlen(stripcslashes($cleaned_quotes_line));
}

echo $total_characters_code - $total_characters_memory;