<?php

/**
 * @Author Nikolay Mikov
 */


$all_file_lines = file("input.txt");

$total_characters_code = 0;
$total_encoded_length = 0;

foreach ($all_file_lines as $line)
{
    $cleaned_line = trim($line);
    $total_characters_code += strlen($cleaned_line);

    $encoded_quotes_line = '"'.addslashes($cleaned_line).'"';
    $total_encoded_length += strlen($encoded_quotes_line);
}

echo $total_encoded_length - $total_characters_code;